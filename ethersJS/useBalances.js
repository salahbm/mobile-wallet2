import React, {useContext, useEffect, useMemo, useState} from 'react';
import {ethers} from 'ethers';
import {DataContext} from '../App';
import {providerList} from './providerslist';
export const useBalances = () => {
  const {loggedInUser} = useContext(DataContext);
  const [tokenBalance, setTokenBalance] = useState([]);
  const [tokenUSD, setTokenUSD] = useState([]);

  useEffect(() => {
    const fetchBalances = async () => {
      const promises = providerList.map(async provider => {
        if (loggedInUser) {
          try {
            const balance = await ethers
              .getDefaultProvider(provider.url)
              .getBalance(loggedInUser.address);

            const formatted = ethers.utils.formatUnits(balance, 'ether');
            const rounded = Number.parseFloat(formatted).toFixed(3);
            // return rounded;
            setTokenBalance(rounded);
          } catch (error) {
            console.log('Error fetching balance:', error);
            return 'Error';
          }
        } else {
          return 'Error';
        }
      });
    };

    fetchBalances();
  }, [loggedInUser, providerList]);

  useEffect(() => {
    async function getBalanceInUSD() {
      try {
        const exchangeRate = await fetch(
          'https://api.coingecko.com/api/v3/simple/price?ids=ethereum&vs_currencies=usd',
        )
          .then(response => response.json())
          .then(data => data.ethereum.usd);
        const balanceInUSD = (tokenBalance * exchangeRate).toFixed(3);
        setTokenUSD(balanceInUSD);
      } catch (error) {
        console.log(error);
      }
    }
    getBalanceInUSD();
  }, [tokenBalance]);
  return [tokenBalance, tokenUSD];
};
